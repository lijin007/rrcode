% Soft Huber with Gaussian Prior
close all;
clear all;
clc;

addpath([cd '/../database/']);
addpath([cd '/../utilities']);

load(['AR_DR_DAT']);
% AR Dateset 
% the mat 'AR_DAT_DAT'contains four data:
% NewTrain_DAT: the training data in Session 1
% NewTest_DAT:  the testing data in Session 2
% trainlabels:  the training data's labels
% testlabels:   the testing data's labels
% the image size is 60*43, without any preprocessing

numTrain = 700;  
numTest = 20;
matTrainData = NewTrain_DAT(:,1:numTrain);
vecTrainLabel = trainlabels(1:numTrain);
matTestData = NewTest_DAT(:,1:numTest);
vecTestLabel = testlabels(1:numTest);
numClass = length(unique(vecTrainLabel));

numIter = 4;    % maximum iteration, usually only need 2 iterations
vecError = [];
imageHight = 60;
imageWidth = 43;
% vecDimensionImage = [30,54,120,100];
vecDimensionImage = [30];
vecLambda = [0.001];
vecMedc = [0.1];

vecBeta = [8];
thresholdWeight = 5e-2;


ori_D = matTrainData./ repmat(sqrt(sum(matTrainData.*matTrainData)),[size(matTrainData,1) 1]);
matIdentity = eye(numTrain);

[disc_set,disc_value,Mean_Image]=funEigenface(matTrainData,200);
disc_value       =   sqrt((disc_value));
mean_x           =   Mean_Image+0.001*disc_set*disc_value';



for iDimension = 1:length(vecDimensionImage)
    dimensionImage = vecDimensionImage(iDimension);
	[disc_set,disc_value,Mean_Image]=funEigenface(matTrainData,dimensionImage);

	for iLambda = 1:length(vecLambda)
    	vecLambda = vecLambda(iLambda);
    	for iMedc = 1:length(vecMedc)
        	median_c   =  vecMedc(iMedc);
        	beta_c     =  vecBeta(1);
        	matLabelPredictionIter = zeros(length(vecTestLabel),numIter);
            matSparsityPerTestIter = zeros(length(vecTestLabel),numIter);       

			for indexTest = 1:numTest
        		fprintf(['Now: ' num2str(indexTest) '\n'])
        		vecError = (matTestData(:,indexTest)-mean_x).^2;
        		vecErrorSort = sort(vecError);
        		medianError = vecErrorSort(ceil(median_c*length(vecError))); 

        		beta = medianError;
        		gata = beta;
        		w = .5 * sqrt( gata ./ ( beta + vecError ) );

        		ori_y             =  matTestData(:,indexTest);
        		norm_y            =  norm(ori_y,2);
        		ori_y             =  ori_y./norm(ori_y,2);
        		er_pref          =   10000000000;            
                
                tic
        		for nit = 1: numIter
        
        			Ori_Train_DAT      =  repmat(w,[1 numTrain]).* matTrainData;
        			Ori_Test_DAT       =  w.* matTestData(:,indexTest);
        			Ori_Train_DAT      =  Ori_Train_DAT./ repmat(sqrt(sum(Ori_Train_DAT.*Ori_Train_DAT)),[size(Ori_Train_DAT,1) 1]);
        			Ori_Test_DAT       =  Ori_Test_DAT./ norm(Ori_Test_DAT);

        			Train_DAT         =  disc_set'*Ori_Train_DAT;
        			Test_DAT          =  disc_set'*Ori_Test_DAT;

        			D                 =  Train_DAT;
        			D                 =  D./ repmat(sqrt(sum(D.*D)),[size(D,1) 1]);
        			y                 =  Test_DAT;
        			y                 =  y./norm(y);

        			x = (D'*D + vecLambda*matIdentity )\(D'*y);
        
        			w_pre = w;

        			vecError           =   norm_y^2*(ori_y-ori_D*x).^2;
        			vecErrorSort      =   sort(vecError);
        			medianError               =   vecErrorSort(ceil(median_c*length(vecError))); 
        			medianError               =   min(medianError,er_pref);
        			er_perf            =   medianError;

        			beta = medianError;
        			gata = beta;
        			w = .5 * sqrt( gata ./ ( beta + vecError ) );

        			if norm(w-w_pre, 2)/norm(w_pre, 2) < thresholdWeight
            			break;
        			end
        
        			gap1 = zeros(numClass,1);
        			for class = 1:numClass
         				temp_s  =  x (vecTrainLabel == class);
         				z1      =  y-D(:,vecTrainLabel == class)*temp_s;
         				gap1(class) = z1(:)'*z1(:);
        			end
        			index = find(gap1==min(gap1));
        			matLabelPredictionIter(indexTest,nit) = index(1);
                    
                    matSparsityPerTestIter(indexTest,nit) = calSparse(x);
                    
        
        		end % num of iteration
                
        		toc
			end % index of test


			reco_rate = zeros(1, numIter);
            
            for ite_re = 1:nit
				reco_rate(ite_re) = sum(matLabelPredictionIter(:,ite_re)' == vecTestLabel)/numTest;
            end

			[reco_rate_max, max_i] = max(reco_rate);
            
            sparsity = mean( matSparsityPerTestIter(:,4) );

			fid = fopen(['RecognitionRate4SH_Gau_AR_Noocclusion.txt'],'a');
			fprintf(fid,'%s\n','-----------------------------------------------------------');
			fprintf(fid,'%s%8f\n','nit = ',nit,'numTest = ',numTest);
			fprintf(fid,'%s%8f%s%8f%s%8f\n','dimensionImage = ',dimensionImage,...
    		'   vecLambda = ',vecLambda,'   beta_a = ',beta_c);
			fprintf(fid,'%s%8f%s%8f\n','median_a = ', median_c,...
    		'   recognition rate = ', reco_rate_max);
			fprintf(fid,'%s%8f\n','max_reco_ite = ',max_i);
            fprintf(fid,'%s%8f\n\n','sparsity = ',sparsity);
			fclose(fid);

    	end % vecMedc
	 end % vecLambda
end % vecDimensionImage
